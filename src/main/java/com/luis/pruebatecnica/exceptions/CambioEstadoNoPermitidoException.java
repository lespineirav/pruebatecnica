package com.luis.pruebatecnica.exceptions;

public class CambioEstadoNoPermitidoException extends Exception {

    private static final long serialVersionUID = -6904646838507218483L;
    

    public CambioEstadoNoPermitidoException(final String message) {
        super(message);
    }
}
