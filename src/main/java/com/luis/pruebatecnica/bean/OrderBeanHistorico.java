package com.luis.pruebatecnica.bean;

import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;

public class OrderBeanHistorico {

    private Integer id;
    private String fechaHoraCambioEstado;
    private String fechaHoraInsercionEstado;
    private ESTADO_SEGUIMIENTO estadoActualizado;

    public OrderBeanHistorico() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFechaHoraCambioEstado() {
        return this.fechaHoraCambioEstado;
    }

    public void setFechaHoraCambioEstado(final String fechaHoraCambioEstado) {
        this.fechaHoraCambioEstado = fechaHoraCambioEstado;
    }

    public String getFechaHoraInsercionEstado() {
        return this.fechaHoraInsercionEstado;
    }

    public void setFechaHoraInsercionEstado(final String fechaHoraInsercionEstado) {
        this.fechaHoraInsercionEstado = fechaHoraInsercionEstado;
    }

    public ESTADO_SEGUIMIENTO getEstadoActualizado() {
        return this.estadoActualizado;
    }

    public void setEstadoActualizado(final ESTADO_SEGUIMIENTO estadoActualizado) {
        this.estadoActualizado = estadoActualizado;
    }

    @Override
    public String toString() {
        return "OrderBeanHistorico{" +
                "id=" + this.id +
                ", fechaHoraCambioEstado='" + this.fechaHoraCambioEstado + '\'' +
                ", fechaHoraInsercionEstado='" + this.fechaHoraInsercionEstado + '\'' +
                ", estadoActualizado=" + this.estadoActualizado.name() +
                '}' + "\n";
    }
}
