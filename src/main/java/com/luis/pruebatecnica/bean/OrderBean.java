package com.luis.pruebatecnica.bean;

import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;

public class OrderBean {

    private Integer orderId;
    private ESTADO_SEGUIMIENTO trackingStatus;
    private String changeStatusDate;

    public OrderBean() {
    }

    public Integer getOrderId() {
        return this.orderId;
    }

    public void setOrderId(final Integer orderId) {
        this.orderId = orderId;
    }

    public ESTADO_SEGUIMIENTO getTrackingStatus() {
        return this.trackingStatus;
    }

    public void setTrackingStatus(final ESTADO_SEGUIMIENTO trackingStatusId) {
        this.trackingStatus = trackingStatusId;
    }

    public String getChangeStatusDate() {
        return this.changeStatusDate;
    }

    public void setChangeStatusDate(final String changeStatusDate) {
        this.changeStatusDate = changeStatusDate;
    }

    @Override
    public String toString() {
        return "OrderBean{" +
                "orderId=" + this.orderId +
                ", trackingStatus=" + this.trackingStatus +
                ", changeStatusDate='" + this.changeStatusDate + '\'' +
                '}' + "\n";
    }
}
