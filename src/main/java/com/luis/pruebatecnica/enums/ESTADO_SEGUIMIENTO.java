package com.luis.pruebatecnica.enums;

public enum ESTADO_SEGUIMIENTO {

    RECOGIDO_ALMACEN(1, "Recogido almacen"),

    EN_REPARTO(2, "Recogido almacen"),

    INCIDENCIA_EN_ENTREGA(3, "Recogido almacen"),

    ENTREGADO(4, "Recogido almacen");

    private final Integer id;
    private final String descripcion;

    ESTADO_SEGUIMIENTO(final Integer id, final String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }


    public Integer getId() {
        return this.id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public static ESTADO_SEGUIMIENTO getById(final Integer id) {
        for (final ESTADO_SEGUIMIENTO estadoSeguimiento : ESTADO_SEGUIMIENTO.values()) {
            if (estadoSeguimiento.getId().equals(id)) {
                return estadoSeguimiento;
            }
        }

        return null;
    }

}
