package com.luis.pruebatecnica.adapters;

import com.luis.pruebatecnica.bean.OrderBean;
import com.luis.pruebatecnica.dto.OrderDTO;
import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class Adapter {

    public static List<OrderBean> adaptToOrderListBean(final List<OrderDTO> orderDTOList) {
        final List<OrderBean> result = new ArrayList<>();

        if (!CollectionUtils.isEmpty(orderDTOList)) {
            orderDTOList.forEach(orderDTO -> result.add(adaptToOrderBean(orderDTO)));
        }

        return result;
    }

    private static OrderBean adaptToOrderBean(final OrderDTO orderDTO) {
        final OrderBean orderBean = new OrderBean();

        if (orderDTO != null) {
            final ESTADO_SEGUIMIENTO trackingStatus = ESTADO_SEGUIMIENTO.getById(orderDTO.getTrackingStatusId());

            orderBean.setOrderId(orderDTO.getOrderId());
            orderBean.setTrackingStatus(trackingStatus);
            orderBean.setChangeStatusDate(orderDTO.getChangeStatusDate());
        }

        return orderBean;
    }
}
