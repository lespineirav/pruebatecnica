package com.luis.pruebatecnica.boot;

import com.luis.pruebatecnica.bean.OrderBean;
import com.luis.pruebatecnica.bean.OrderBeanHistorico;
import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"com.luis.pruebatecnica"})
public class PruebatecnicaApplication {

    // Lista de los pedidos con el estado actual en memoria
    public static List<OrderBean> orderBeanList;
    // Mapa de historico de los con idPedido de clave
    public static Map<Integer, List<OrderBeanHistorico>> orderBeanHistoricoMapByOrderId;

    public static void main(final String[] args) {
        SpringApplication.run(PruebatecnicaApplication.class, args);

        // Se inicializan por defecto las actualizaciones de los pedidos pasados en el ejemplo del pdf para trabajar
        // con ellos en memoria
        orderBeanList = new ArrayList<>();
        orderBeanHistoricoMapByOrderId = new HashMap<>();

    }

    private static List<OrderBean> inicializarOrderBeanListEnMemoria() {
        final List<OrderBean> orderBeanList = new ArrayList<>();

        orderBeanList.add(crearOrderBeanInicial(230688716, 1));
        orderBeanList.add(crearOrderBeanInicial(230688717, 2));
        orderBeanList.add(crearOrderBeanInicial(230688718, 3));

        return orderBeanList;
    }

    private static OrderBean crearOrderBeanInicial(final Integer orderId, final Integer trackingStatusId) {
        final OrderBean orderBean = new OrderBean();

        orderBean.setOrderId(orderId);
        orderBean.setTrackingStatus(ESTADO_SEGUIMIENTO.getById(trackingStatusId));

        return orderBean;
    }

}
