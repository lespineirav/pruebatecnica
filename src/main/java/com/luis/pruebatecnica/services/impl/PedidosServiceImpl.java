package com.luis.pruebatecnica.services.impl;

import com.luis.pruebatecnica.adapters.Adapter;
import com.luis.pruebatecnica.bean.OrderBean;
import com.luis.pruebatecnica.boot.PruebatecnicaApplication;
import com.luis.pruebatecnica.business.CambioEstadosBsnss;
import com.luis.pruebatecnica.dto.OrderDTO;
import com.luis.pruebatecnica.manager.PedidosManager;
import com.luis.pruebatecnica.services.PedidosService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PedidosServiceImpl implements PedidosService {

    @Autowired(required = false)
    private PedidosManager pedidosManager;

    @Override
    public void actualizarPedidos(final List<OrderDTO> orderDTOList) {
        final List<OrderBean> orderBeanList = Adapter.adaptToOrderListBean(orderDTOList);
        final List<OrderBean> orderBeanListEnMemoria = PruebatecnicaApplication.orderBeanList;
        final CambioEstadosBsnss cambioEstadosBsnss = CambioEstadosBsnss.getInstance();

        for (final OrderBean orderBeanConCambios : CollectionUtils.emptyIfNull(orderBeanList)) {
            // Pedido correspondiente en la lista de pedidos en memoria
            final OrderBean orderBeanMemoria = obtenerOrderBeanDeListaMemoria(orderBeanListEnMemoria,
                    orderBeanConCambios);

            // Se comprueba que el cambio de estado del pedido sea correcto. Si es null se permite cambiar el estado
            final boolean esCorrectoCambioDeEstado = orderBeanMemoria == null ||
                    cambioEstadosBsnss.esCorrectoCambioDeEstado(orderBeanConCambios.getTrackingStatus(),
                            orderBeanMemoria.getTrackingStatus());

            // Si el cambio de estado es correcto se procesa el pedido
            if (esCorrectoCambioDeEstado) {
                this.pedidosManager.procesarPedido(orderBeanConCambios, orderBeanMemoria, orderBeanListEnMemoria);
            }
        }

        System.out.println("DATOS ACTUALES\n" + orderBeanListEnMemoria + "--------------------------------");
        System.out.println("\n" + "HISTORICO\n" + PruebatecnicaApplication.orderBeanHistoricoMapByOrderId
                + "-------------------------------");
        System.out.println();

    }

    private OrderBean obtenerOrderBeanDeListaMemoria(final List<OrderBean> orderBeanListMemoria, final OrderBean orderBeanToSearch) {
        final Optional<OrderBean> orderbean = CollectionUtils.emptyIfNull(orderBeanListMemoria).stream()
                .filter(orderBeanMemoria -> orderBeanMemoria.getOrderId().equals(orderBeanToSearch.getOrderId()))
                .findFirst();

        return orderbean != null && !orderbean.isEmpty() ? orderbean.get() : null;

    }


}
