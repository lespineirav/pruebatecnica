package com.luis.pruebatecnica.services;

import com.luis.pruebatecnica.dto.OrderDTO;

import java.util.List;

public interface PedidosService {

    void actualizarPedidos(final List<OrderDTO> orderDTOList);

}
