package com.luis.pruebatecnica.business;

import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;

public class CambioEstadosBsnss {

    private static CambioEstadosBsnss instance;

    public static CambioEstadosBsnss getInstance() {
        return instance == null ? new CambioEstadosBsnss() : instance;
    }

    public boolean esCorrectoCambioDeEstado(final ESTADO_SEGUIMIENTO nuevoEstado, final ESTADO_SEGUIMIENTO antiguoEstado) {
        // Si aun no esta el pedido en memoria el cambio de estado es correcto
        final boolean aunNoExistePedido = antiguoEstado == null;
        if (aunNoExistePedido) {
            return true;
        }

        // El estado "RECOGIDO EN ALMACEN" es un estado inicial, no se puede transitar a este estado desde el resto de
        // estados.
        final boolean esPasoEstadoRecogidoEnAlmacenCorrecto = esPasoEstadoRecogidoEnAlmacenCorrecto(nuevoEstado, antiguoEstado);

        // El estado "ENTREGADO" es un estado final, una vez un pedido alcance ese estado se descartaran el resto de
        // actualizaciones de seguimiento que recibamos.
        final boolean estaPedidoEnMemoriaEnEstadoEntregado = pedidoEnMemoriaEstaEnEstadoEntregado(antiguoEstado);
        final boolean estadoActualizarNoEsRecogidoEnAlmacen = !ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN.equals(nuevoEstado);

        return esPasoEstadoRecogidoEnAlmacenCorrecto ||
                (!estaPedidoEnMemoriaEnEstadoEntregado && estadoActualizarNoEsRecogidoEnAlmacen);
    }

    private boolean esPasoEstadoRecogidoEnAlmacenCorrecto(final ESTADO_SEGUIMIENTO estadoAModificar, final ESTADO_SEGUIMIENTO estadoActual) {
        // Si el estado a modificar es RECOGIDO_ALMACEN y el estado actual es otro, entonces no se puede cambiar
        final boolean estadoAModificarEsRecogidoAlmacen = ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN.equals(estadoAModificar);
        final boolean estadoActualEsDiferenteARecogidoAlmacen = !ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN.equals(estadoActual);

        return estadoAModificarEsRecogidoAlmacen && !estadoActualEsDiferenteARecogidoAlmacen;


    }

    private boolean pedidoEnMemoriaEstaEnEstadoEntregado(final ESTADO_SEGUIMIENTO estadoSeguimientoPedidoMemoria) {
        return ESTADO_SEGUIMIENTO.ENTREGADO.equals(estadoSeguimientoPedidoMemoria);
    }

}
