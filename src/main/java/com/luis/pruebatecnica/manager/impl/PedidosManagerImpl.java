package com.luis.pruebatecnica.manager.impl;

import com.luis.pruebatecnica.bean.OrderBean;
import com.luis.pruebatecnica.bean.OrderBeanHistorico;
import com.luis.pruebatecnica.boot.PruebatecnicaApplication;
import com.luis.pruebatecnica.manager.PedidosManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class PedidosManagerImpl implements PedidosManager {

    @Override
    public void procesarPedido(final OrderBean orderBeanConCambios, final OrderBean orderBeanMemoria,
                               final List<OrderBean> orderBeanListEnMemoria) {
        String fechaHoraActualizacion = null;
        if (orderBeanMemoria != null) {
            // Si hay que actualizar un pedido existente se actualiza
            fechaHoraActualizacion = actualizarPedido(orderBeanConCambios, orderBeanMemoria);
        } else {
            // Si el pedido no existia simplemente se inserta desde 0
            fechaHoraActualizacion = anadirNuevoPedido(orderBeanConCambios, orderBeanListEnMemoria);
        }


        // Se anade al historico de los pedidos
        anadirHistoricoPedido(orderBeanConCambios, fechaHoraActualizacion);
    }

    private String actualizarPedido(final OrderBean orderBeanConCambios, final OrderBean orderBeanMemoria) {
        orderBeanMemoria.setChangeStatusDate(orderBeanConCambios.getChangeStatusDate());
        orderBeanMemoria.setTrackingStatus(orderBeanConCambios.getTrackingStatus());

        return new Date().toString();
    }

    private String anadirNuevoPedido(final OrderBean orderBeanConCambios, final List<OrderBean> orderBeanListEnMemoria) {
        orderBeanListEnMemoria.add(orderBeanConCambios);

        return new Date().toString();
    }


    private void anadirHistoricoPedido(final OrderBean orderBeanConCambios, final String fechaHoraActualizacion) {
        final Map<Integer, List<OrderBeanHistorico>> orderBeanHistoricoMapByOrderId = PruebatecnicaApplication.orderBeanHistoricoMapByOrderId;

        // Busco la lista de historico del pedido
        final List<OrderBeanHistorico> orderBeanList = MapUtils.emptyIfNull(orderBeanHistoricoMapByOrderId)
                .get(orderBeanConCambios.getOrderId());

        // Si no habia registros en el historico se inicializa la lista
        if (CollectionUtils.isEmpty(orderBeanList)) {
            final List<OrderBeanHistorico> orderBeanHistoricoList = new ArrayList<>();
            // Se anade al historial el primer historico en caso de que se haya metido un pedido que no existia
            orderBeanHistoricoList.add(obtenerOrderHistoricoDeOrderActualizado(orderBeanConCambios,
                    fechaHoraActualizacion));
            orderBeanHistoricoMapByOrderId.put(orderBeanConCambios.getOrderId(), orderBeanHistoricoList);

        } else {
            // Si ya existia algun registro se crea el nuevo registro en el historico del pedido
            final OrderBeanHistorico orderBeanHistorico = obtenerOrderHistoricoDeOrderActualizado(orderBeanConCambios,
                    fechaHoraActualizacion);

            orderBeanList.add(orderBeanHistorico);
        }
    }


    private OrderBeanHistorico obtenerOrderHistoricoDeOrderActualizado(final OrderBean orderBeanConCambios, final String fechaHoraActualizacion) {
        final OrderBeanHistorico orderBeanHistorico = new OrderBeanHistorico();
        orderBeanHistorico.setId(orderBeanConCambios.getOrderId());
        orderBeanHistorico.setFechaHoraCambioEstado(orderBeanConCambios.getChangeStatusDate());
        orderBeanHistorico.setFechaHoraInsercionEstado(fechaHoraActualizacion);
        orderBeanHistorico.setEstadoActualizado(orderBeanConCambios.getTrackingStatus());
        return orderBeanHistorico;
    }
}
