package com.luis.pruebatecnica.manager;

import com.luis.pruebatecnica.bean.OrderBean;

import java.util.List;

public interface PedidosManager {

    void procesarPedido(OrderBean orderBeanConCambios, OrderBean orderBeanMemoria, List<OrderBean> orderBeanListEnMemoria);
    
}
