package com.luis.pruebatecnica.dto;

import java.io.Serializable;

public class OrderDTO implements Serializable {

    private static final long serialVersionUID = -3987044427524819632L;

    private Integer orderId;
    private Integer trackingStatusId;
    private String changeStatusDate;

    public OrderDTO() {
    }

    public Integer getOrderId() {
        return this.orderId;
    }

    public void setOrderId(final Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getTrackingStatusId() {
        return this.trackingStatusId;
    }

    public void setTrackingStatusId(final Integer trackingStatusId) {
        this.trackingStatusId = trackingStatusId;
    }

    public String getChangeStatusDate() {
        return this.changeStatusDate;
    }

    public void setChangeStatusDate(final String changeStatusDate) {
        this.changeStatusDate = changeStatusDate;
    }
}
