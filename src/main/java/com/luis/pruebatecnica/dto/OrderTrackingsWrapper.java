package com.luis.pruebatecnica.dto;

import java.io.Serializable;
import java.util.List;

public class OrderTrackingsWrapper implements Serializable {
    private static final long serialVersionUID = 5835622337677431047L;

    List<OrderDTO> orderTrackings;

    public OrderTrackingsWrapper() {
    }

    public List<OrderDTO> getOrderTrackings() {
        return this.orderTrackings;
    }

    public void setOrderTrackings(final List<OrderDTO> orders) {
        this.orderTrackings = orders;
    }
}
