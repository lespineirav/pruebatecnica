package com.luis.pruebatecnica.controllers;

import com.luis.pruebatecnica.dto.OrderDTO;
import com.luis.pruebatecnica.dto.OrderTrackingsWrapper;
import com.luis.pruebatecnica.services.PedidosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
public class PedidosController {

    @Autowired(required = false)
    private PedidosService pedidosService;

    @RequestMapping(value = "/tracking", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    ResponseEntity updateOrders(@RequestBody final OrderTrackingsWrapper orderTrackingsWrapper) {
        // TODO luisev anadir excepciones si hay tiempo
        final List<OrderDTO> orderDTOList = orderTrackingsWrapper.getOrderTrackings();

        this.pedidosService.actualizarPedidos(orderDTOList);


        return null;
    }
}
