package com.luis.pruebatecnica.business;

import com.luis.pruebatecnica.enums.ESTADO_SEGUIMIENTO;
import org.junit.jupiter.api.*;

class CambioEstadosBsnssTest {

    private CambioEstadosBsnss cambioEstadosBsnss;

    @BeforeEach
    void setupEach() {
        this.cambioEstadosBsnss = CambioEstadosBsnss.getInstance();
    }

    @Nested
    class EsCorrectoCambioDeEstado {

        @DisplayName("Cuando se cambia al estado RECOGIDO_ALMACEN desde otro estado => devuelve false")
        @Test
        void cuandoCambioARecogidoAlmacenDesdeOtroEstadoDevuelveFalse() {
            final boolean esCorrectoCambioDeEstadoEnRepartoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN, ESTADO_SEGUIMIENTO.EN_REPARTO);
            final boolean esCorrectoCambioDeEntregadoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN, ESTADO_SEGUIMIENTO.ENTREGADO);
            final boolean esCorrectoCambioDeEstadoIncidenciaRepartoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN, ESTADO_SEGUIMIENTO.INCIDENCIA_EN_ENTREGA);

            Assertions.assertFalse(esCorrectoCambioDeEstadoEnRepartoARecogidoAlmacen);
            Assertions.assertFalse(esCorrectoCambioDeEntregadoARecogidoAlmacen);
            Assertions.assertFalse(esCorrectoCambioDeEstadoIncidenciaRepartoARecogidoAlmacen);
        }

        @DisplayName("Cuando un pedido esta en estado ENTREGADO y se cambia a otro cualquiera => devuelve false")
        @Test
        void cuandoPedidoEntregadoYCambiaEstadoDevuelveFalse() {
            final boolean esCorrectoCambioDeEstadoEnRepartoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN, ESTADO_SEGUIMIENTO.ENTREGADO);
            final boolean esCorrectoCambioDeEntregadoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.EN_REPARTO, ESTADO_SEGUIMIENTO.ENTREGADO);
            final boolean esCorrectoCambioDeEstadoIncidenciaRepartoARecogidoAlmacen = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.INCIDENCIA_EN_ENTREGA, ESTADO_SEGUIMIENTO.ENTREGADO);

            Assertions.assertFalse(esCorrectoCambioDeEstadoEnRepartoARecogidoAlmacen);
            Assertions.assertFalse(esCorrectoCambioDeEntregadoARecogidoAlmacen);
            Assertions.assertFalse(esCorrectoCambioDeEstadoIncidenciaRepartoARecogidoAlmacen);
        }

        @DisplayName("Cuando un pedido no esta en estado ENTREGADO y se pasa a ENTREGADO => devuelve true")
        @Test
        void cuandoPedidoNoEntregadoYPasaAEntregadoDevuelveTrue() {
            final boolean esCorrectoCambioDeEstadoEnRepartoAEntregado = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.ENTREGADO, ESTADO_SEGUIMIENTO.EN_REPARTO);
            final boolean esCorrectoCambioDeIncidenciaRepartoAEntregado = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.ENTREGADO, ESTADO_SEGUIMIENTO.INCIDENCIA_EN_ENTREGA);
            final boolean esCorrectoCambioDeEstadoRecogidoAlmacenAEntregado = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.ENTREGADO, ESTADO_SEGUIMIENTO.RECOGIDO_ALMACEN);

            Assertions.assertTrue(esCorrectoCambioDeEstadoEnRepartoAEntregado);
            Assertions.assertTrue(esCorrectoCambioDeIncidenciaRepartoAEntregado);
            Assertions.assertTrue(esCorrectoCambioDeEstadoRecogidoAlmacenAEntregado);

        }

        @DisplayName("Resto de cambios entre estados => devuelve true")
        @Test
        void cuandoCambioEntreRestoDeEstadosDevuelveTrue() {
            final boolean esCorrectoCambioDeEstadoAEnReparto = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.EN_REPARTO, ESTADO_SEGUIMIENTO.INCIDENCIA_EN_ENTREGA);
            final boolean esCorrectoCambioDeEstadoAincidenciaEnEntrega = CambioEstadosBsnssTest.this.cambioEstadosBsnss
                    .esCorrectoCambioDeEstado(ESTADO_SEGUIMIENTO.INCIDENCIA_EN_ENTREGA, ESTADO_SEGUIMIENTO.EN_REPARTO);

            Assertions.assertTrue(esCorrectoCambioDeEstadoAEnReparto);
            Assertions.assertTrue(esCorrectoCambioDeEstadoAincidenciaEnEntrega);

        }
    }
}